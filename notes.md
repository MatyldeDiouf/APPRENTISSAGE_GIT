[Git tutorial for beginners, YT](https://www.youtube.com/watch?v=8JJ101D3knE&t=42s&ab_channel=ProgrammingwithMosh)

## Version control system

Version control system records changes made to a code over time in a database called **repository**.  

## Configuration

+ System: settings apply to all users of the current computer
+ Global: setting apply to all repositories of the current user
+ Local: settings apply to the current repository

Ex: ``git config --global setting_to_configure``, settings might be username, user email, default editor...  
Global configuration file location: ``~/.gitconfig``  

## Help

Any command + ``--help``, ex: ``git config --help``  

## Basics

### Initializing a repository

+ Create new directory ``mkdir Project``
+ Initialize empty git repository ``git init``, contains info about the project's history (branches, info, objects...)

### Git workflow

Project directory contains the git repository (hidden sub-directory).  
Creating a commit is like **taking a snapshot** of the project at a given moment.  

**Staging area/index:** intermediate step enabling us to review the modified files before commiting changes. The snapshot/commit will be permanently stored in the git repository. Unwanted changes can be unstaged (and committed as another snapshot).  
The ``git add`` command adds files to the staging area, git "tracks" those files. "Untracked files" are files not yet added to the staging area.  

Project directory &rightarrow; Staging area &rightarrow; Git repository  

The ``git commit`` command creates a commit with a unique ID, basic infos (message, author, date, time) and a complete snapshot of the project (full content yet compressed) at a certain time, **committing does not clear the staging area**.  

### Staging files

When working in the directory, the ``git status`` command gives the status of the current directory (commits, untracked and modified files).  
``git add`` can take multiple files, patterns (ex: ``*.txt``), "``.`` " (= entire directory) as argument.  

``git commit -a -m "message"``: the staging area can be skipped by committing directly and using the ``-a`` (all) option.  

### Committing changes

``git commit -m "message"``: the message should be meaningful and explaining the modifications in the snapshot.  
The point of commiting is to record checkpoints as we go. Each commit should represent a logically separate change set (2 different commits for a typo and a bug fix, not the 2 together).  
Convention: use the present tense.  

### Deleting files

A deleted file (in the project directory) should also be added to the staging area to "update" it, otherwise it will exist in the staging area but not in the workspace.  
The ``git ls-files`` command lists the files in the staging area.  

The ``git rm`` command removes files from both working directory and staging area, it can take multiple files, patterns, "``.`` " as argument.  

### Renaming and moving files

When renaming a file, both the new and former files (with different names) must be added to the staging area.  
The ``git mv`` command renames/moves files from both working directory and staging area, it can take multiple files, patterns, "``.`` " as argument.  

### Ignoring files

A ``.gitignore`` file should be created in the root of the project. This file lists every file or directory that should not be staged when adding to the staging area.  

When **accidentally tracking/committing a file/change** that should not have been, modify it then ``git rm --cached -r file`` to remove it recursively from the staging area. Then add it to the ``.gitignore`` file so git no longer tracks it.  

### Viewing staged and unstaged changes

When a file is staged, before committing, reviewing the changes might be necessary.  
The ``git diff --staged`` command enables you to compare the former (from the last commit) and the new (from the staging area) copies of the files in the staging area **where lines have been modified**.  

+ "``-``" represents changes in the former file
+ "``+``" represents changes in the new file
+ Header = line starting with ``@@``, represents the file's parts that were changed (in chunks)

Ex: ``@@ -1,3 +1,5 @@``, in the header, the first part prefixed by ``-`` gives info about the former copy, the second part prefixed by ``+`` gives info about the new staged copy:

+ ``-1,3`` starting from line 1, 3 lines were extracted and are shown underneath
+ ``+1,5``: starting from line 1, 5 lines were extracted and shown underneath (in green and prefixed by ``+``)

The ``git diff`` command enables you to see unstaged changes by comparing the former (from the staging area) and new (from the working directory) copies of files (no output when you stage your changes correctly).  

### Viewing the history

The ``git log`` command enables you to access your commits history (several options).  
The ``git log --oneline`` option shows the unique identifiers of all commits.  

### Viewing a commit

"HEAD" represents the current commit you're viewing, by default, the most recent commit made.  

You can access a commit's content with ``git show``:  

+ ``git show unique_id``: using a commit's unique identifier
+ ``git show HEAD``: shows the last commit
+ ``git show HEAD~X``: X is a number and represents the n° of steps we wanna go back from the first commit (``git show HEAD~1`` will show the second to last commit)

These commands will show infos about the commit and a diff output.  

The ``git show HEAD~1:full_path_to_the_file`` command enables you to see the exact code version stored in the commit.  

The ``git ls-tree HEAD~1`` command lists all the files and directories stored in this specific commit ("blob" is file, "tree" is directory).  

### Unstaging files

The command ``git restore --staged file`` command enables you to restore files from the staging area to the working directory while leaving actual modifications untouched, it can take multiple files, patterns, "``.``" as argument.  
The ``git restore file`` command restore the file to its last snapshot and discards uncommitted (local) changes.  

### Discarding local changes

The ``git clean`` command clears untracked files (requires additionnal arguments).  

### Restoring a file to an earlier version

From the history, the ``git restore --source=HEAD~X`` command enables you to restore a file to a previous version by using its associated commit.
